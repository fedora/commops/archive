include::ROOT:partial$attributes.adoc[]

= Ambassador Mentor
# Ambassador Mentorship Process

## The Process for Mentorship
* Candidates approach with interest
    * Write to Ambassador Mailing List 
    * Discourse thread 

* Ambassador Rep to Mindshare works with interested folks and schedules a call for 2-3 weeks out.
    * The purpose of the call:
       * candidates ask general questions
       * interacts with a mentor on call 
       * clarify doubts during application process
       * review funding eligibility documentation (mindshare - under development)
    * Candidates move ahead :
       * Apply by opening ticket on Ambassador repo (can be public or private). Viewable by FaMa & Ambassador Mentors. 
       * Tickets need to include this information:
          * FAS 
          * User WIKI Page
          * Preferred email address
          * Answers to the Ambassador questionnaire

## Application Process

* The application process is async and self paced. An applicant can makes a copy of [https://docs.google.com/document/d/1-cP5hbdwiMxQEFs1-6DhFEnlRXHTAffq21sFAfiDzKw/edit] or copy the question from a docs.fp.o.[TO BE PUT IN SEPARATE PAGE]

* Once the answers are finalized and a ticket is opened.
  * An applicant will have to wait for evaluation

* There is a two week follow up period wherein Ambassador mentors or FaMa can ask any questions and for further details. 

* Mentor can choose to have a 1x1 interview or a IRC chat or simply feel satisfied enough by reading answers and few questions.



