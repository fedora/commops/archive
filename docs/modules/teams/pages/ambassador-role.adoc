include::ROOT:partial$attributes.adoc[]

= Ambassador Team Member

A Fedora Ambassador is a representative of the Fedora Project. Ambassadors ensure the public understand Fedora Project’s principles and the work that community is doing. Additionally Ambassadors are responsible for helping to grow the contributor base, and to act as a liaison between other FLOSS projects and the Fedora community. You can still represent Fedora in the public without being an Ambassador. In this case you are a https://docs.fedoraproject.org/en-US/commops/teams/ambassador-role/[Fedora Advocate].

*  You give a face and a name to Fedora. You are a person, friendly and approachable. Fedora can be big and scary, but you are going to help make sure that one person finds their way through all the confusion into the place where they can contribute.
   
*  You are the glue. You help connect different people in different parts of the Project because you are always dealing with people whose interests are different. You should be eager and happy to work with other parts of Fedora to solve problems.

* People are the key. Everything that we do – events, budgets, swag, membership verifications, blogging – it’s all done as an attempt to make personal connections with folks who either want to use or contribute to Fedora. Places, where Ambassadors get together, are one of the few times that Fedora contributors have an opportunity to meet face to face (other than FUDCons and FADs).

* You are the expert in your region. You understand the culture, the needs, and the best ways to communicate about Fedora and FOSS in your region. FAmSCo and other “leaders” in Fedora work for you, and need to listen to what you are saying makes sense. We can’t ignore the global picture, but we need to be flexible in adapting to local needs.

== How to move to Ambassador Emeritus

Write a post on the https://discussion.fedoraproject.org/tag/ambassadors[Ambassador Discourse] topic and let the team know that you are ready to step down as an Ambassador and move to the Ambassador Emeritus group. If you have set tasks/responsibilities related to Ambassadors, ensure that you have a successor in place.

== How does someone come back?

Write a post on the Ambassador discourse to express your interest in returning. Join the next Monthly Ambassador call and raise your discourse thread. If you took a short break, and there are no objections, you can expect to be added back to the Ambassador group shortly. If you took a longer break, we would suggest finding a mentor and getting a short refresher before returning. Ambassador Rep to Mindshare, the FCAIC, and representatives from FAMSCo will look at the request and make final approval.

== Ambassador Conduct

The Fedora Code of Conduct apply to everyone in the Fedora community, and especially to you as a Fedora Ambassador.

When acting as ambassador, you don’t just represent yourself, you represent the Fedora Project. When people think of you, they will think of the Fedora Project. This means that there are some important rules to follow, and giving the right impression is important setting aside personal biases in favor of goodwill for yourself and for Fedora.

=== Know your message

As an ambassador, and as part of the Marketing project, you need to know the good word of Fedora, and know the differences between it and its cousin, Red Hat Enterprise Linux. Make sure to focuses on the strengths of Fedora, do not insult any other project or product.

=== No monetary requests

Remember, you are not allowed to publish any monetary details or funding requests on fedoraproject.org. You are not authorized to collect funds on behalf of the Fedora Project or for any Fedora-related venture.

=== Respect the Trademark Guidelines

Always be aware of the Trademark Guidelines for the Fedora trademarks. They protect the Fedora brand. You should also respect the logo guidelines. As an ambassador, you should be especially careful not to violate these guidelines.

=== No illegal software

You should not direct people to software that may violate the law. Review and understand our Forbidden items. You can direct users to third-party software, but do not lead users to software that may violate the law or copyrights. When you direct someone to third-party software, please make it clear that use of the software is not supported or encouraged by the Fedora Project.

=== You represent Fedora, not Red Hat

This is pretty simple. You represent the Fedora Project, which is sponsored by Red Hat. You do not represent Red Hat. If someone is interested in Red Hat specific details or Red Hat Enterprise Linux, please put them in touch with the Fedora Project Leader. Even though you may believe that Fedora is perfection, you should not insult any other project or product.

=== Dress to represent

The Fedora brand is independent, and stand-alone, and it’s important to Fedora’s marketing efforts that we maintain that distinction. When representing Fedora in person, try not to confuse people by wearing Red Hat, or any other distribution, clothing or apparel.

Our founding organization and main sponsor, Red Hat, has specifically requested that we not connect hats, red in color or otherwise, to the Fedora Project. Please do not use a hat, whether it’s a fedora or any other type of hat, in any presentation of Fedora or to represent the Fedora Project in any way.

=== Online presence

As an Ambassador you are representing the Fedora Project, which may include virtual spaces. In any virtual space that you are representing yourself as a Fedora representative you need to adhere to Fedora’s Code of Conduct.

=== Ask for help if you get lost

Inevitably, things happen, and they aren’t always great. If you need help or something goes wrong, make sure to reach out to any local event coordinators, or group leads, and also communicate the situation to the Fedora Community Action and Impact Coordinator.

== Responsibilities

The main responsibility of Ambassadors is to ensure the Fedora updates are being broadcasted to the public and collect feedback from users and contributors. Ambassadors should share their feedback at the Mindshare Committee through their representative at the Mindshare Committee.

- Organize Fedora participation at events (sessions, info booths, hand out swag, etc)
- On board new contributors and users
- Organize release parties and install fests
-  Talk about the Fedora Four Foundations
- Regularly check up on other Ambassadors in order to ensure others are doing good and provide assistance
- Promote FLOSS and open source projects
- Demonstrate Fedora and other open source projects to the public
- Promote Fedora at local user groups with talks etc…

== Teams you will be closely working with

=== Ambassadors work closely with:

    - https://docs.fedoraproject.org/en-US/mindshare-committee/[the Mindshare Committee]
    - https://docs.fedoraproject.org/en-US/fedora-join/[the Join SIG]
    - https://docs.fedoraproject.org/en-US/marketing/[the Marketing team]
    - https://docs.fedoraproject.org/en-US/commops/contribute/commops-landing/[CommOps]
    - https://docs.fedoraproject.org/en-US/commops/teams/ambassador-role/[Advocates]

== Contact information

- Discourse forum: https://discussion.fedoraproject.org/tag/ambassadors
- IRC channel:  #fedora-ambassadors on Libera.Chat
- Matrix/Element: https://matrix.to/#/#fedora-ambassadors:fedora.im
